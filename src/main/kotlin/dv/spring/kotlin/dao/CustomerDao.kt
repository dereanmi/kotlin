package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus

interface CustomerDao{
    fun getCustomers():List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndDesc(name: String, email: String): List<Customer>
    fun getCustomerByProvince(province: String?): List<Customer>
    fun getCustomerByPartialUserStatus(userStatus: UserStatus): List<Customer>
    fun save(customer: Customer): Customer
    fun findById(id: Long): Customer?
//    fun getCustomerByProductName(name: String): List<Customer>
}
