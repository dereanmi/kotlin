package dv.spring.kotlin.entity

import javax.persistence.*

@Entity
data class Customer(
        override var name: String? = null,
        override var email: String? = null,
        override var userStatus: UserStatus? = UserStatus.PEDING,
        var isDeleted: Boolean = false
) : User(name, email, userStatus) {

    @ManyToMany
    var shippingAddress = mutableListOf<Address>()
    @ManyToOne
    var billingAddress: Address? = null
    @ManyToOne
    var defaultAddress: Address? = null
}
