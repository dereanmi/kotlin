package dv.spring.kotlin.service

import dv.spring.kotlin.dao.AddressDao
import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.AbstractPersistable_.id
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CustomerServiceImpl : CustomerService {
    @Transactional
    override fun remove(id: Long): Customer? {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }


    @Autowired
    lateinit var addressDao: AddressDao
    override fun save(customer: Customer): Customer {
        val address = customer.defaultAddress?.let { addressDao.save(it) }
        val customer = customerDao.save(customer)
        customer.defaultAddress = address
        return customer
    }

    @Transactional
    override fun save(AddressId:Long, customer: Customer): Customer{
        val address = addressDao.findById(AddressId)
        val customer = customerDao.save(customer)
        customer.defaultAddress = address
        return customer
    }


//    override fun getCustomerByProductName(name: String): List<Customer> {
//        return customerDao.getCustomerByProductName(name)
//    }

    override fun getCustomerByPartialUserStatus(userStatus: UserStatus): List<Customer> {
        return customerDao.getCustomerByPartialUserStatus(userStatus)
    }

    override fun getCustomerByProvince(province: String?): List<Customer> {
        return customerDao.getCustomerByProvince(province)
    }

    override fun getCustomerByPartialNameAndDesc(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByPartialNameAndDesc(name, email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }

    @Autowired
    lateinit var customerDao: CustomerDao

    override fun getCustomerByName(name: String): Customer? = customerDao.getCustomerByName(name)


    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }
}